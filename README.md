# PROJECT KGB

L’objectif est de créer un site internet permettant la gestion des données du KGB.

## Prérequis

    Dans la console : <code> npm install </code>

## Technologies utilisées

    * HTML5
    * CSS3
    * Bootstrap
    * JS
    * PHP
    * SQL

## Outils utilisés

    * Node.js

## AUTEURS

    * Jean Mionnet & Déb Phoenix ~ *good vibes only*
    * Proudly students powered by: http://simplon.co


**La [maquette du projet](https://www.figma.com/file/r2IXqvds6y2vLNGtkxC52w/KGB?node-id=0%3A1) a été réalisée sur figma**.