<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KGB - Administration</title>
    <link rel="icon" href="./prod/assets/images/incognito.svg"/>

    <!-- Primary Meta Tags -->
    <meta name="title" content="KGB - Administration">
    <meta name="description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.kgb.io/">
    <meta property="og:title" content="KGB - Administration">
    <meta property="og:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="og:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.kgb.io/">
    <meta property="twitter:title" content="KGB - Administration">
    <meta property="twitter:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="twitter:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- CSS -->
    <link rel="stylesheet" href="./prod/style.css"/>
</head>
<body>

    <!-- PAGE DE CONNEXION -->
    <div id="admin_home_page">
        <!-- HEADER BRAND -->
        <div id="admin_header">
            <a href="./home.php" id="admin_header_label">KGB</a>
        </div>
        <!-- HEADER SECTION -->
        <div id="admin_header_section">
            <h3 id="admin_header_section_label">ADMINISTRATION</h3>
        </div>
        
        <!-- BUTTONS -->
        <div id="buttons_panel">
            <div id="buttons">
                <!-- LIST BUTTON BOX -->
                <div id="admin_list_box">
                    <!-- LIST BUTTON ICON/LINK -->
                    <a href="./admin_list.php" id="admin_list_icon">
                        <svg height="128" width="128" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512.18 512.18" style="enable-background:new 0 0 512.18 512.18;" xml:space="preserve"><path fill="#EDD933" d="M448.18,80h-320c-17.673,0-32,14.327-32,32s14.327,32,32,32h320c17.673,0,32-14.327,32-32S465.853,80,448.18,80z"/><path fill="#EDD933" d="M64.18,112c-0.036-8.473-3.431-16.586-9.44-22.56c-12.481-12.407-32.639-12.407-45.12,0C3.61,95.414,0.215,103.527,0.18,112c-0.239,2.073-0.239,4.167,0,6.24c0.362,2.085,0.952,4.124,1.76,6.08c0.859,1.895,1.876,3.715,3.04,5.44c1.149,1.794,2.49,3.457,4,4.96c1.456,1.45,3.065,2.738,4.8,3.84c1.685,1.227,3.512,2.248,5.44,3.04c2.121,1.1,4.382,1.908,6.72,2.4c2.073,0.232,4.167,0.232,6.24,0c8.45,0.007,16.56-3.329,22.56-9.28c1.51-1.503,2.851-3.166,4-4.96c1.164-1.725,2.181-3.545,3.04-5.44c1.021-1.932,1.826-3.971,2.4-6.08C64.419,116.167,64.419,114.073,64.18,112z"/><path fill="#EDD933" d="M64.18,256c0.238-2.073,0.238-4.167,0-6.24c-0.553-2.065-1.359-4.053-2.4-5.92c-0.824-1.963-1.843-3.839-3.04-5.6c-1.109-1.774-2.455-3.389-4-4.8c-12.481-12.407-32.639-12.407-45.12,0C3.61,239.414,0.215,247.527,0.18,256c0.062,4.217,0.875,8.388,2.4,12.32c0.802,1.893,1.766,3.713,2.88,5.44c1.217,1.739,2.611,3.348,4.16,4.8c1.414,1.542,3.028,2.888,4.8,4c1.685,1.228,3.511,2.249,5.44,3.04c1.951,0.821,3.992,1.412,6.08,1.76c2.047,0.459,4.142,0.674,6.24,0.64c2.073,0.239,4.167,0.239,6.24,0c2.036-0.349,4.024-0.94,5.92-1.76c1.981-0.786,3.861-1.807,5.6-3.04c1.772-1.112,3.386-2.458,4.8-4c1.542-1.414,2.888-3.028,4-4.8c1.23-1.684,2.251-3.51,3.04-5.44c1.093-2.124,1.9-4.384,2.4-6.72C64.426,260.167,64.426,258.073,64.18,256z"/><path fill="#EDD933" d="M64.18,400c0.237-2.073,0.237-4.167,0-6.24c-0.553-2.116-1.359-4.157-2.4-6.08c-0.859-1.895-1.876-3.715-3.04-5.44c-1.112-1.772-2.458-3.386-4-4.8c-12.481-12.407-32.639-12.407-45.12,0c-1.542,1.414-2.888,3.028-4,4.8c-1.164,1.725-2.181,3.545-3.04,5.44c-0.83,1.948-1.421,3.99-1.76,6.08c-0.451,2.049-0.665,4.142-0.64,6.24c0.036,8.473,3.431,16.586,9.44,22.56c1.414,1.542,3.028,2.888,4.8,4c1.685,1.228,3.511,2.249,5.44,3.04c1.951,0.821,3.992,1.412,6.08,1.76c2.047,0.459,4.142,0.674,6.24,0.64c2.073,0.239,4.167,0.239,6.24,0c2.036-0.349,4.024-0.94,5.92-1.76c1.981-0.786,3.861-1.807,5.6-3.04c1.772-1.112,3.386-2.458,4.8-4c1.542-1.414,2.888-3.028,4-4.8c1.231-1.683,2.252-3.51,3.04-5.44c1.092-2.125,1.899-4.384,2.4-6.72C64.426,404.167,64.426,402.073,64.18,400z"/><path fill="#EDD933" d="M480.18,224h-352c-17.673,0-32,14.327-32,32s14.327,32,32,32h352c17.673,0,32-14.327,32-32S497.853,224,480.18,224z"/><path fill="#EDD933" d="M336.18,368h-208c-17.673,0-32,14.327-32,32c0,17.673,14.327,32,32,32h208c17.673,0,32-14.327,32-32C368.18,382.327,353.853,368,336.18,368z"/></svg>
                    </a>
                    <!-- LIST BUTTON LABEL -->
                    <p class="buttons_label text-center">ACCEDER AUX DONNEES</p>
                </div>
                <!-- PLUS BUTTON BOX -->
                <div id="admin_plus_box">
                    <a href="./add_mission.php" id="admin_plus_icon">
                        <svg height="128" viewBox="0 0 426.66667 426.66667" width="128" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m405.332031 192h-170.664062v-170.667969c0-11.773437-9.558594-21.332031-21.335938-21.332031-11.773437 0-21.332031 9.558594-21.332031 21.332031v170.667969h-170.667969c-11.773437 0-21.332031 9.558594-21.332031 21.332031 0 11.777344 9.558594 21.335938 21.332031 21.335938h170.667969v170.664062c0 11.777344 9.558594 21.335938 21.332031 21.335938 11.777344 0 21.335938-9.558594 21.335938-21.335938v-170.664062h170.664062c11.777344 0 21.335938-9.558594 21.335938-21.335938 0-11.773437-9.558594-21.332031-21.335938-21.332031zm0 0"/></svg>
                    </a>
                    <p class="buttons_label text-center">AJOUTER UNE MISSION</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BOOTSTRAP -->
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>