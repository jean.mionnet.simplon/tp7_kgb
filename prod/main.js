// CHANGE LE BOUTON BURGER A L'OUVERTURE DU MENU DEROULANT
$("#burger").click(() => {
    $("#modal_menu").toggleClass("active");

    if ($("#modal_menu").hasClass("active")) {
        $("#burger").html('<svg height="40" viewBox="0 0 329.26933 329" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>');
    } else {
        $("#burger").html('<svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>');
    }
})

// OUVRE LES FORMULAIRES D'AJOUT DE DATA
$("#add_data_agents").click(() => {
    $("#form_agent_box").css('display', 'flex');
})

$("#add_data_contacts").click(() => {
    $("#form_contact_box").css('display', 'flex');
})

$("#add_data_planques").click(() => {
    $("#form_planque_box").css('display', 'flex');
})

$("#add_data_cibles").click(() => {
    $("#form_cible_box").css('display', 'flex');
})

$("#add_data_specialites").click(() => {
    $("#form_specialite_box").css('display', 'flex');
})

$("#add_data_pays").click(() => {
    $("#form_pays_box").css('display', 'flex');
})

// OUVRE LES FORMULAIRES D'EDITION

// FERME LES MODALES LORS DU CLIC SUR LA CROIX
$(".cross").click(() => {
    $("#form_agent_box").css('display', 'none');
    $("#form_agent_edit_box").css('display', 'none');
    $("#form_contact_box").css('display', 'none');
    $("#form_planque_box").css('display', 'none');
    $("#form_cible_box").css('display', 'none');
    $("#form_specialite_box").css('display', 'none');
    $("#form_pays_box").css('display', 'none');
    // VIDER LE FORMULAIRE ?? A TESTER
})

// SUPPRIME DES ELEMENTS DES LISTES
$(".delete").click((e) => {
    $(e.target).closest(".item").remove();
})
