<?php
    require_once  './Manager/HomeMissionsManager.php';

    $home_mission_manager = new HomeMissionsManager();

    $home_missions = $home_mission_manager->getAll();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KGB - Accueil</title>
    <link rel="icon" href="./prod/assets/images/incognito.svg"/>

    <!-- Primary Meta Tags -->
    <meta name="title" content="KGB - Accueil">
    <meta name="description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.kgb.io/">
    <meta property="og:title" content="KGB - Accueil">
    <meta property="og:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="og:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.kgb.io/">
    <meta property="twitter:title" content="KGB - Accueil">
    <meta property="twitter:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="twitter:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- CSS -->
    <link rel="stylesheet" href="./prod/style.css"/>
</head>

<body>
    <!-- BARRE DE NAVIGATION -->
    <nav id="home_nav" class="navbar navbar-expand-lg justify-content-between">
        <!-- LOGO -->
        <a id="home_nav_brand" class="navbar-brand" href="./home.php">KGB</a>
        <!-- ANCRES AFFICHES SEULEMENT EN DESKTOP -->
        <div id="home_nav_links" class="d-none d-md-block">
            <a href="./home_admin.php" class="mr-5">ADMINISTRATION</a>
            <a href="./connexion.php">DECONNEXION</a>
        </div>
        <!-- BURGER AFFICHE EN TABLETTE/MOBILE -->
        <div id="burger" class="d-md-none d-block">
            <svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>
        </div>
    </nav>

    <!-- MODAL MENU -->
    <div id="modal_menu">
        <div id="modal_links" class="d-none d-md-block d-flex flex-column justify-content-center">
            <div class="text-center mb-4">
                <a href="./home_admin.php">ADMINISTRATION</a>
            </div>
            <div class="text-center">
                <a href="./connexion.php">DECONNEXION</a>
            </div>
        </div>
    </div>

    <!-- MISSIONS CARDS DECK -->
    <div class="container-fluid" id="missions_deck">
        <div class="missions_deck">
            <!-- BOUCLE DANS LES MISSIONS POUR TOUTE LES AFFICHER -->
            <?php
                foreach ($home_missions as $mission) {
            ?>
            <!-- MISSION CARD -->
            <a class="mission" href="./data_mission.php?mission=<?= $mission->getId() ?>&agent=<?= $mission->getIdAgent()?>&type=<?= $mission->getTypeMission()?>&statut=<?= $mission->getStatut()?>">
                <!-- MISSION CARD SVG -->
                <svg id="mission_closed" class="mt-2 ml-3 mb-3" height="64" width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <polygon fill="#EDD933" points="339.392,258.624 512,367.744 512,144.896"/> <polygon fill="#EDD933" points="0,144.896 0,367.744 172.608,258.624"/><path fill="#EDD933" d="M480,80H32C16.032,80,3.36,91.904,0.96,107.232L256,275.264l255.04-168.032C508.64,91.904,495.968,80,480,80z"/><path fill="#EDD933" d="M310.08,277.952l-45.28,29.824c-2.688,1.76-5.728,2.624-8.8,2.624c-3.072,0-6.112-0.864-8.8-2.624l-45.28-29.856L1.024,404.992C3.488,420.192,16.096,432,32,432h448c15.904,0,28.512-11.808,30.976-27.008L310.08,277.952z"/></svg>
                <svg id="mission_opened" class="mt-2 ml-3 mb-3" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="64" height="64" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"><g class="layer"><polygon id="svg_1" points="340.3919982910156,307.6239929199219 513,416.7439880371094 513,193.89599609375 " fill="#EDD933"/><polygon id="svg_2" points="1,193.89599609375 1,416.7439880371094 173.60800170898438,307.6239929199219 " fill="#EDD933"/><path fill="#EDD933" d="m481,-14l-448,0c-15.968,0 -28.64,11.904 -31.04,27.232l255.04,168.032l255.04,-168.032c-2.4,-15.328 -15.072,-27.232 -31.04,-27.232z" id="svg_3" transform="rotate(180 257,83.63197326660156) "/><path id="svg_4" d="m311.08,326.952l-45.28,29.824c-2.688,1.76 -5.728,2.624 -8.8,2.624c-3.072,0 -6.112,-0.864 -8.8,-2.624l-45.28,-29.856l-200.896,127.072c2.464,15.2 15.072,27.008 30.976,27.008l448,0c15.904,0 28.512,-11.808 30.976,-27.008l-200.896,-127.04z" fill="#EDD933"/></g></svg>           
                <!-- MISSION TITLE -->
                <h2 class="text-center mb-3"><?= $mission->getTitre()?></h2>
                <!-- MISSION INFORMATIONS (A GENERER EN PHP) -->
                <p class="ml-3">Type... <?= $mission->getTypeMission()?></p>
                <p class="ml-3">Agent... <?= $mission->getPrenom() . " " . $mission->getNom()?></p>
                <p class="ml-3 mb-4">Statut... <?= $mission->getStatut()?></p>
            </a>

            <?php
            }
            ?>
        </div>
    </div>

    <!-- BOOTSTRAP -->
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- JS -->
    <script src="./prod/main.js"></script>
</body>
</html>