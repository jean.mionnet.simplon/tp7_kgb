<!-- CONFIG -->
<?php

require_once './config.php';

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KGB - Connexion</title>
    <link rel="icon" href="./prod/assets/images/incognito.svg"/>

    <!-- Primary Meta Tags -->
    <meta name="title" content="KGB - Connexion">
    <meta name="description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.kgb.io/">
    <meta property="og:title" content="KGB - Connexion">
    <meta property="og:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="og:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.kgb.io/">
    <meta property="twitter:title" content="KGB - Connexion">
    <meta property="twitter:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="twitter:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- CSS -->
    <link rel="stylesheet" href="./prod/style.css"/>
</head>
<body>

    <!-- PAGE DE CONNEXION -->
    <div id="connexion_page">
        <form class="form-inline p-4 p-md-5" id="connexion_form" method="POST">
            <div id="connexion_header">
                <p id="connexion_header_label">CONNEXION</p>
            </div>
            <!-- FORMULAIRE -->
            <div class="form-row">
                <div class="col-9">
                    <label for="input_email" class="sr-only">Email</label>
                    <input type="email" name="email" class="form-control mr-3" id="input_email" placeholder="Email..." required>
                </div>
                <!-- BOUTON SUBMIT -->
                <div class="col-3">
                    <button id="connexion_form_submit" type="submit" class="btn form-control" name="send_email"><svg height="24" width="24" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 448 448" style="enable-background:new 0 0 448 448;" xml:space="preserve"><polygon fill="#EDD933" points="0.213,32 0,181.333 320,224 0,266.667 0.213,416 448,224"/></svg></button>
                </div>
            </div>
        </form>
    </div>
    <!-- FORM SUBMIT -->
    <?php

    if (isset($_POST['send_email'])) {
        $connexion = new PDO(SERVER . ';dbname=' . DBNAME . ';port=' . PORT . ';charset=utf8mb4', USERNAME , MDP);
        $email = htmlspecialchars($_POST['email']);
        $req = $connexion->prepare('SELECT * FROM administrateurs WHERE email = :email');
        $req->execute(array('email' => $email));
        $resultat = $req->fetch();

        if(!empty($resultat)) {
            header("Location: password.php?email=".$email);
        } else { ?>
            <script>confirm("Cet utilisateur n'existe pas")</script>
            <?php
        }
    }

    ?>

    <!-- BOOTSTRAP -->
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>