
<!-- CONFIG -->
<?php

    require_once './config.php';

?>

<!-- CONNEXION -->
<?php

    try {
        $connexion = new PDO(SERVER . ';dbname=' . DBNAME . ';port=' . PORT . ';charset=utf8mb4', USERNAME , MDP);
    } catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }

?>

<!-- FORMULAIRE SPECIALITE INSERT -->
<?php
    if (isset($_POST['send_specialite'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO specialite (libelle_specialite) VALUES (?)');

            $specialite = $_POST['specialite'];

            $insert->execute(array($specialite));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

<!-- FORMULAIRE PAYS INSERT -->
<?php
    if (isset($_POST['send_pays'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO pays (libelle_pays, nationalite) VALUES (?,?)');

            $libelle_pays = $_POST['libelle_pays'];
            $nationalite = $_POST['nationalite'];

            $insert->execute(array($libelle_pays, $nationalite));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

<!-- FORMULAIRE CIBLE INSERT -->
<?php
    if (isset($_POST['send_cible'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO cible (nom, prenom, date_naissance, nationalite) VALUES (?,?,?,?)');

            $nom = $_POST['name_cible'];
            $prenom = $_POST['prenom_cible'];
            $date = $_POST['date_cible'];
            $nationalite = $_POST['nationalite_cible'];

            $insert->execute(array($nom, $prenom, $date, $nationalite));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

<!-- FORMULAIRE CONTACT INSERT -->
<?php
    if (isset($_POST['send_contact'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO contact (nom, prenom, date_naissance, nationalite) VALUES (?,?,?,?)');

            $nom = $_POST['name_contact'];
            $prenom = $_POST['prenom_contact'];
            $date = $_POST['date_contact'];
            $nationalite = $_POST['nationalite_contact'];

            $insert->execute(array($nom, $prenom, $date, $nationalite));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

<!-- FORMULAIRE AGENT INSERT -->
<?php
    if (isset($_POST['send_agent'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO agent (nom, prenom, date_naissance, nationalite, specialite) VALUES (?,?,?,?,?)');

            $nom = $_POST['name_agent'];
            $prenom = $_POST['prenom_agent'];
            $date = $_POST['date_agent'];
            $nationalite = $_POST['nationalite_agent'];
            $specialite = $_POST['specialite_agent'];

            $insert->execute(array($nom, $prenom, $date, $nationalite, $specialite));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

<!-- FORMULAIRE PLANQUE INSERT -->
<?php
    if (isset($_POST['send_planque'])) {
        try {
            $insert = $connexion->prepare('INSERT INTO planque (adresse, pays, num_type_planque) VALUES (?,?,?)');

            $adresse = $_POST['adresse'];
            $pays = $_POST['pays'];
            $type = $_POST['type'];

            $insert->execute(array($adresse, $pays, $type));
            echo "insertion OK";

            header('Location: admin_list.php');
        } catch (PDOException $e) {
            die("pas inséré : " . $e->getMessage());
        }
    }
?>

