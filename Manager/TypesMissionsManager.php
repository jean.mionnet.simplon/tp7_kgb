<?php

require_once './Manager/DBManager.php';
require_once './Modeles/TypeMission.php';

class TypeMissionManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM type_mission');

        while($row = $stmt->fetch()) {
            $type_mission = new TypeMission();
            $type_mission->setNumTypeMission($row['id_type_mission']);
            $type_mission->setLibelleTypeMission($row['libelle_type_mission']);
            $result[] = $type_mission;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}