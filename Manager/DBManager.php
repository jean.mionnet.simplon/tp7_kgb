
<?php

require_once './config.php';

class DBManager {
    private $connexion;
    public function __construct()
    {
        $this->connexion = new PDO(SERVER . ';dbname=' . DBNAME . ';port=' . PORT . ';charset=utf8mb4', USERNAME , MDP);
    }

    public function getConnexion() {
        return $this->connexion;
    }
}

?>