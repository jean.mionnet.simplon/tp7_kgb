<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Contact.php';

class ContactsManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM contact');

        while($row = $stmt->fetch()) {
            $contact = new contact();
            $contact->setNomDeCode($row['id_contact']);
            $contact->setNom($row['nom']);
            $contact->setPrenom($row['prenom']);
            $contact->setDateDeNaissance($row['date_naissance']);
            $contact->setNationalite($row['nationalite']);
            $result[] = $contact;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}