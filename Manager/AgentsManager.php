<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Agent.php';

class AgentsManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM agent');

        while($row = $stmt->fetch()) {
            $agent = new Agent();
            $agent->setCodeAgent($row['id_agent']);
            $agent->setNom($row['nom']);
            $agent->setPrenom($row['prenom']);
            $agent->setDateDeNaissance($row['date_naissance']);
            $agent->setNationaliteAgent($row['nationalite']);
            $agent->setSpecialite($row['specialite']);
            $result[] = $agent;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}