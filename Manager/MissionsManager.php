<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Mission.php';

class MissionsManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM mission');

        while($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setCodeMission($row['id_mission']);
            $mission->setNumStatut($row['statut']);
            $mission->setTitre($row['titre']);
            $mission->setCodePays($row['pays']);
            $mission->setNumTypeMission($row['type_mission']);
            $mission->setDescription($row['description']);
            $mission->setCodeAgent($row['agent']);
            $mission->setCodeContact($row['contact']);
            $mission->setCodeCible($row['cible']);
            $mission->setCodePlanque($row['planque']);
            $mission->setDateDebut($row['date_debut']);
            $mission->setDateFin($row['date_fin']);
            $mission->setNumSpecialite($row['specialite']);
            $result[] = $mission;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}