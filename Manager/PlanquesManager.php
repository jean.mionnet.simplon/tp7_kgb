<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Planque.php';

class PlanquesManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM planque');

        while($row = $stmt->fetch()) {
            $planque = new planque();
            $planque->setCodePlanque($row['id_planque']);
            $planque->setAdresse($row['adresse']);
            $planque->setLibellePays($row['pays']);
            $planque->setNumTypePlanque($row['num_type_planque']);
            $result[] = $planque;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}