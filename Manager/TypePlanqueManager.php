<?php

require_once './Manager/DBManager.php';
require_once './Modeles/TypePlanque.php';

class TypePlanqueManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM type_planque');

        while($row = $stmt->fetch()) {
            $type_planque = new Typeplanque();
            $type_planque->setNumTypePlanque($row['id_type_planque']);
            $type_planque->setLibelleTypePlanque($row['libelle_type_planque']);
            $result[] = $type_planque;
        }

        return $result;
    }

/*     public function add($planque) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO planques VALUES description = :code');
        $stmt->execute(['code' => $planque->getCode()]);
        return true;
    } */
}