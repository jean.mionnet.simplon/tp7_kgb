<?php

require_once './Manager/DBManager.php';
require_once './Modeles/HomeMission.php';

class HomeMissionsManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT DISTINCT 
            id_statut, id_agent, id_mission, titre, nom, prenom, libelle_statut, libelle_type_mission
            FROM mission
        JOIN statut
            ON mission.statut = statut.id_statut
        JOIN type_mission
            ON mission.type_mission = type_mission.id_type_mission 
        JOIN agent
            ON agent.id_agent = mission.agent');

        while($row = $stmt->fetch()) {
            $mission = new HomeMission();
            $mission->setIdAgent($row['id_agent']);
            $mission->setId($row['id_mission']);
            $mission->setTitre($row['titre']);
            $mission->setTypeMission($row['libelle_type_mission']);
            $mission->setStatut($row['libelle_statut']);
            $mission->setStatut($row['libelle_statut']);
            $mission->setNom($row['nom']);
            $mission->setPrenom($row['prenom']);
            $result[] = $mission;
        }

        return $result;
    }
}