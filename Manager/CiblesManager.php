<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Cible.php';

class CiblesManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM cible');

        while($row = $stmt->fetch()) {
            $cible = new Cible();
            $cible->setNomDeCode($row['id_cible']);
            $cible->setNom($row['nom']);
            $cible->setPrenom($row['prenom']);
            $cible->setDateDeNaissance($row['date_naissance']);
            $cible->setNationalite($row['nationalite']);
            $result[] = $cible;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}