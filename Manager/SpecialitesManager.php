<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Specialite.php';

class SpecialiteManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM specialite');

        while($row = $stmt->fetch()) {
            $specialite = new specialite();
            $specialite->setNumSpecialite($row['id_specialite']);
            $specialite->setLibelleSpecialite($row['libelle_specialite']);
            $result[] = $specialite;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}