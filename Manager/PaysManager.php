<?php

require_once './Manager/DBManager.php';
require_once './Modeles/Pays.php';

class PaysManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM pays');

        while($row = $stmt->fetch()) {
            $pays = new pays();
            $pays->setCodePays($row['id_pays']);
            $pays->setLibellePays($row['libelle_pays']);
            $pays->setNationalite($row['nationalite']);
            $result[] = $pays;
        }

        return $result;
    }

/*     public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO Missions VALUES description = :code');
        $stmt->execute(['code' => $mission->getCode()]);
        return true;
    } */
}