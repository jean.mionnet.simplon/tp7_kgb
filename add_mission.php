<!-- OBJETS NECESSAIRES AUX LISTES -->
<?php

require_once './Manager/MissionsManager.php';
require_once './Manager/TypesMissionsManager.php';
require_once './Manager/TypePlanqueManager.php';
require_once './Manager/AgentsManager.php';
require_once './Manager/ContactsManager.php';
require_once './Manager/PlanquesManager.php';
require_once './Manager/TypePlanqueManager.php';
require_once './Manager/CiblesManager.php';
require_once './Manager/SpecialitesManager.php';
require_once './Manager/PaysManager.php';

$mission_manager = new MissionsManager();
$type_mission_manager = new TypeMissionManager();
$agent_manager = new AgentsManager();
$contact_manager = new ContactsManager();
$planque_manager = new PlanquesManager();
$type_planque_manager = new TypePlanqueManager();
$cible_manager = new CiblesManager();
$specialite_manager = new SpecialiteManager();
$pays_manager = new PaysManager();

$missions = $mission_manager->getAll();
$types_mission = $type_mission_manager->getAll();
$agents = $agent_manager->getAll();
$contacts = $contact_manager->getAll();
$planques = $planque_manager->getAll();
$types_planque = $type_planque_manager->getAll();
$cibles = $cible_manager->getAll();
$specialites = $specialite_manager->getAll();
$payss = $pays_manager->getAll();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KGB Administration - Ajout mission</title>
    <link rel="icon" href="./prod/assets/images/incognito.svg"/>

    <!-- Primary Meta Tags -->
    <meta name="title" content="KGB Administration - Ajout mission">
    <meta name="description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.kgb.io/">
    <meta property="og:title" content="KGB Administration - Ajout mission">
    <meta property="og:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="og:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.kgb.io/">
    <meta property="twitter:title" content="KGB Administration - Ajout mission">
    <meta property="twitter:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="twitter:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- CSS -->
    <link rel="stylesheet" href="./prod/style.css"/>
</head>
<body>
<!-- CONTENU PRINCIPAL ADMINISTRATION - DATA -->
<div id="admin_home_page">
    <!-- HEADER BRAND -->
    <div id="admin_header">
        <a href="./home.php" id="admin_header_label">KGB</a>
    </div>
    <!-- HEADER SECTION -->
    <div id="admin_header_section">
        <h3 id="admin_header_section_label">ADMINISTRATION</h3>
    </div>

    <!-- BREADCRUMB -->
    <nav class="ml-5 mt-5" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb--style">
            <li class="breadcrumb-item"><a href="./home.php">Accueil</a></li>
            <li class="breadcrumb-item"><a href="./home_admin.php">Administration</a></li>
            <!-- remplacer par du php -->
            <li class="breadcrumb-item active" aria-current="page">Ajouter une mission</li>
        </ol>
    </nav>

    <!-- INPUTS -->
    <div id="lists" class="scrollable">
        <form class="pl-0" method="POST" action="add_mission_check.php">

            <!-- TITRE -->
            <div class="form-group">
                <label for="titre">Titre<span class="required"> *</span></label>
                <input type="text" class="form-control" id="titre" name="titre" placeholder="Entrer le titre de la mission..." required>
            </div>

            <!-- DESCRIPTION -->
            <div class="form-group">
                <label for="description">Description<span class="required"> *</span></label>
                <input type="textarea" class="form-control" id="description" name="description" placeholder= "Décrire la mission..." required>
            </div>

            <!-- SELECT TYPE -->
            <div class="form-group">
                <label for="type">Type de mission<span class="required"> *</span></label>
                <select name="type" id="type" class="form-control" required>
                    <option value="">-- Type --</option>
                    <?php
                    foreach ($types_mission as $type_mission) {
                        ?>
                        <option value="<?= $type_mission->getNumTypeMission() ?>"><?= $type_mission->getLibelleTypeMission() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT PAYS -->
            <div class="form-group">
                <label for="pays">Pays<span class="required"> *</span></label>
                <select name="pays" id="pays" class="form-control" required>
                    <option value="">-- Pays de la mission --</option>
                    <?php
                    foreach ($payss as $pays) {
                        ?>
                        <option value="<?= $pays->getCodePays() ?>"><?= $pays->getLibellePays() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT PLANQUE -->
            <div class="form-group">
                <label for="planque">Planque</label>
                <select name="planque" id="planque" class="form-control">
                    <option value="">-- Planque utilisée --</option>
                    <?php
                    foreach ($planques as $planque) {
                        ?>
                        <option value="<?= $planque->getCodePlanque() ?>"><?= $planque->getAdresse() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT SPECIALITE -->
            <div class="form-group">
                <label for="specialite">Spécialité<span class="required"> *</span></label>
                <select name="specialite" id="specialite" class="form-control" required>
                    <option value="">-- Spécialité requise --</option>
                    <?php
                    foreach ($specialites as $specialite) {
                        ?>
                        <option value="<?= $specialite->getNumSpecialite() ?>"><?= $specialite->getLibelleSpecialite() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT AGENT -->
            <div class="form-group">
                <label for="agent">Agent<span class="required"> *</span></label>
                <select name="agent" id="agent" class="form-control" required>
                    <option value="">-- Agent envoyé --</option>
                    <?php
                    foreach ($agents as $agent) {
                        ?>
                        <option value="<?= $agent->getCodeAgent() ?>"><?= $agent->getPrenom() . " " . $agent->getNom() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT CIBLE -->
            <div class="form-group">
                <label for="cible">Cible<span class="required"> *</span></label>
                <select name="cible" id="cible" class="form-control" required>
                    <option value="">-- Cible --</option>
                    <?php
                    foreach ($cibles as $cible) {
                        ?>
                        <option value="<?= $cible->getNomDeCode() ?>"><?= $cible->getPrenom() . " " . $cible->getNom() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <!-- SELECT CONTACT -->
            <div class="form-group">
                <label for="contact">Contact<span class="required"> *</span></label>
                <select name="contact" id="contact" class="form-control" required>
                    <option value="">-- Contact --</option>
                    <?php
                    foreach ($contacts as $contact) {
                        ?>
                        <option value="<?= $contact->getNomDeCode() ?>"><?= $contact->getPrenom() . " " . $contact->getNom() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>


            <!-- DATE DEBUT -->
            <div class="form-group">
                <label for="date_debut">Date de début<span class="required"> *</span></label>
                <input type="date" class="form-control" id="date_debut" name="date_debut" placeholder= "Date de début" required>
            </div>

            <!-- DATE FIN -->
            <div class="form-group">
                <label for="date_fin">Date de fin</label>
                <input type="date" class="form-control" id="date_fin" name="date_fin" placeholder= "Date de fin">
            </div>

            <!-- SUBMIT -->
            <button type="submit" name="send_mission" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<!-- BOOTSTRAP -->
<script src="./node_modules/jquery/dist/jquery.min.js"></script>
<script src="./node_modules/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- JS -->
<script src="./prod/main.js"></script>
</body>
</html>