<!-- CONFIG -->
<?php

require_once '../config.php';

?>

<!-- REMOVE AGENT -->
<?php
try {
    $connexion = new PDO(SERVER . ';dbname=' . DBNAME . ';port=' . PORT . ';charset=utf8mb4', USERNAME , MDP);
} catch (PDOException $e) {
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

try {
    $request = $connexion->prepare('DELETE FROM mission WHERE id_mission = :id');
    $result = $request->execute(['id' => $_GET['idMission']]);

    if ($result == "") {
        $result = "false";
    } else {
        $result = "delete";
    }

    header("Location: ../admin_list.php?result=".$result);
} catch (PDOException $e) {
    die("pas supprimé : " . $e->getMessage());
}
?>