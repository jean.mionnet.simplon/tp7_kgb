<?php

class TypeMission {
    private $num_type_mission;
    private $libelle_type_mission;

    // GETTERS
    public function getNumTypeMission() {
        return $this->num_type_mission;
    }
    public function getLibelleTypeMission() {
        return $this->libelle_type_mission;
    }
    
    // SETTERS
    public function setNumTypeMission($num) {
        $this->num_type_mission = $num;
    }
    public function setLibelleTypeMission($type) {
        $this->libelle_type_mission = $type;
    }
}