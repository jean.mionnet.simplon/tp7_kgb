<?php

class Cible {
    private $nom_de_code;
    private $nom;
    private $prenom;
    private $date_de_naissance;
    private $nationalite;

    // GETTERS
    public function getNomDeCode() {
        return $this->nom_de_code;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getPrenom() {
        return $this->prenom;
    }
    public function getDateDeNaissance() {
        return $this->date_de_naissance;
    }
    public function getNationalite() {
        return $this->nationalite;
    }
    
    // SETTERS
    public function setNomDeCode($code) {
        $this->nom_de_code = $code;
    }
    public function setNom($name) {
        $this->nom = $name;
    }
    public function setPrenom($firstname) {
        $this->prenom = $firstname;
    }
    public function setDateDeNaissance($born) {
        $this->date_de_naissance = $born;
    }
    public function setNationalite($from) {
        $this->nationalite = $from;
    }
}