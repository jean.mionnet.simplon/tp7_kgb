<?php

class TypePlanque {
    private $num_type_planque;
    private $libelle_type_planque;

    // GETTERS
    public function getNumTypeplanque() {
        return $this->num_type_planque;
    }
    public function getLibelleTypeplanque() {
        return $this->libelle_type_planque;
    }
    
    // SETTERS
    public function setNumTypeplanque($num) {
        $this->num_type_planque = $num;
    }
    public function setLibelleTypeplanque($type) {
        $this->libelle_type_planque = $type;
    }
}