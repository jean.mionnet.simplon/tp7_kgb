<?php

class Mission {
    private $code_mission;
    private $num_statut;
    private $titre;
    private $code_pays;
    private $num_type_mission;
    private $description;
    private $code_agent;
    private $code_contact;
    private $code_cible;
    private $code_planque;
    private $date_debut;
    private $date_fin;
    private $num_specialite;

    // GETTERS
    public function getCodeMission() {
        return $this->code_mission;
    }
    public function getNumStatut() {
        return $this->num_statut;
    }
    public function getTitre() {
        return $this->titre;
    }
    public function getCodePays() {
        return $this->code_pays;
    }
    public function getNumTypeMission() {
        return $this->num_type_mission;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getCodeAgent() {
        return $this->code_agent;
    }
    public function getCodeContact() {
        return $this->code_contact;
    }
    public function getCodeCible() {
        return $this->code_cible;
    }
    public function getCodePlanque() {
        return $this->code_planque;
    }
    public function getDateDebut() {
        return $this->date_debut;
    }
    public function getDateFin() {
        return $this->date_fin;
    }
    public function getNumSpecialite() {
        return $this->num_specialite;
    }
    
    // SETTERS
    public function setCodeMission($code) {
        $this->code_mission = $code;
    }
    public function setNumStatut($statut) {
        $this->num_statut = $statut;
    }
    public function setTitre($titre) {
        $this->titre = $titre;
    }
    public function setCodePays($pays) {
        $this->code_pays = $pays;
    }
    public function setNumTypeMission($type) {
        $this->num_type_mission = $type;
    }
    public function setDescription($desc) {
        $this->description = $desc;
    }
    public function setCodeAgent($agent) {
        $this->code_agent = $agent;
    }
    public function setCodeContact($contact) {
        $this->code_contact = $contact;
    }
    public function setCodeCible($cible) {
        $this->code_cible = $cible;
    }
    public function setCodePlanque($planque) {
        $this->code_planque = $planque;
    }
    public function setDateDebut($debut) {
        $this->date_debut = $debut;
    }
    public function setDateFin($fin) {
        $this->date_fin = $fin;
    }
    public function setNumSpecialite($specialite) {
        $this->num_specialite = $specialite;
    }
}