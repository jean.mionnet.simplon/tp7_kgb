<?php

class HomeMission {
    private $id_mission;
    private $statut;
    private $titre;
    private $type_mission;
    private $nom;
    private $prenom;
    private $id_agent;

    // GETTERS
    public function getId() {
        return $this->id_mission;
    }
    public function getStatut() {
        return $this->statut;
    }
    public function getTitre() {
        return $this->titre;
    }
    public function getTypeMission() {
        return $this->type_mission;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getPrenom() {
        return $this->prenom;
    }
    public function getIdAgent() {
        return $this->id_agent;
    }
    
    // SETTERS
    public function setId($id_mission) {
        $this->id_mission = $id_mission;
    }
    public function setStatut($statut) {
        $this->statut = $statut;
    }
    public function setTitre($titre) {
        $this->titre = $titre;
    }
    public function setTypeMission($type) {
        $this->type_mission = $type;
    }
    public function setNom($nom) {
        $this->nom = $nom;
    }
    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }
    public function setIdAgent($id_agent) {
        $this->id_agent = $id_agent;
    }
}