<?php

class Pays {
    private $code_pays;
    private $libelle_pays;
    private $nationalite;

    // GETTERS
    public function getCodePays() {
        return $this->code_pays;
    }
    public function getLibellePays() {
        return $this->libelle_pays;
    }
    public function getNationalite() {
        return $this->nationalite;
    }
    
    // SETTERS
    public function setCodePays($code) {
        $this->code_pays = $code;
    }
    public function setLibellePays($pays) {
        $this->libelle_pays = $pays;
    }
    public function setNationalite($n) {
        $this->nationalite = $n;
    }
}