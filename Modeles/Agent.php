<?php

class Agent {
    private $code_agent;
    private $nom;
    private $prenom;
    private $date_de_naissance;
    private $nationalite;    
    private $specialite;

    // GETTERS
    public function getCodeAgent() {
        return $this->code_agent;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getPrenom() {
        return $this->prenom;
    }
    public function getDateDeNaissance() {
        return $this->date_de_naissance;
    }
    public function getNationalite() {
        return $this->nationalite;
    }
    public function getSpecialite() {
        return $this->specialite;
    }
    
    // SETTERS
    public function setCodeAgent($agent) {
        $this->code_agent = $agent;
    }
    public function setNom($name) {
        $this->nom = $name;
    }
    public function setPrenom($firstname) {
        $this->prenom = $firstname;
    }
    public function setDateDeNaissance($born) {
        $this->date_de_naissance = $born;
    }
    public function setNationaliteAgent($from) {
        $this->nationalite = $from;
    }
    public function setSpecialite($spec) {
        $this->specialite = $spec;
    }
}