<?php

class Planque {
    private $code_planque;
    private $adresse;
    private $libelle_pays;
    private $num_type_planque;

    // GETTERS
    public function getCodePlanque() {
        return $this->code_planque;
    }
    public function getAdresse() {
        return $this->adresse;
    }
    public function getPays() {
        return $this->libelle_pays;
    }
    public function getNumTypePlanque() {
        return $this->num_type_planque;
    }
    
    // SETTERS
    public function setCodePlanque($code) {
        $this->code_planque = $code;
    }
    public function setAdresse($where) {
        $this->adresse = $where;
    }
    public function setLibellePays($label) {
        $this->libelle_pays = $label;
    }
    public function setNumTypePlanque($type) {
        $this->num_type_planque = $type;
    }
}