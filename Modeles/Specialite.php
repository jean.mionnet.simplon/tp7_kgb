<?php

class Specialite {
    private $num_specialite;
    private $libelle_specialite;

    // GETTERS
    public function getNumSpecialite() {
        return $this->num_specialite;
    }
    public function getLibelleSpecialite() {
        return $this->libelle_specialite;
    }
    
    // SETTERS
    public function setNumSpecialite($num) {
        $this->num_specialite = $num;
    }
    public function setLibelleSpecialite($specialite) {
        $this->libelle_specialite = $specialite;
    }
}