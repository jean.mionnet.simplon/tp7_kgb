<?php
    require_once './Manager/MissionsManager.php';
    require_once './Manager/CiblesManager.php';
    require_once './Manager/AgentsManager.php';
    require_once './Manager/ContactsManager.php';
    require_once './Manager/PaysManager.php';
    require_once './Manager/PlanquesManager.php';
    require_once './Manager/SpecialitesManager.php';
    require_once './Manager/TypesMissionsManager.php';
    require_once './Manager/TypePlanqueManager.php';

    $mission_manager = new MissionsManager();
    $cible_manager = new CiblesManager();
    $agent_manager = new AgentsManager();
    $contact_manager = new ContactsManager();
    $pays_manager = new PaysManager();
    $planque_manager = new PlanquesManager();
    $specialite_manager = new SpecialiteManager();
    $type_mission_manager = new TypeMissionManager();
    $type_planque_manager = new TypePlanqueManager();

    $missions = $mission_manager->getAll();
    $cibles = $cible_manager->getAll();
    $agents = $agent_manager->getAll();
    $contacts = $contact_manager->getAll();
    $payss = $pays_manager->getAll();
    $planques = $planque_manager->getAll();
    $specialites = $specialite_manager->getAll();
    $types_missions = $type_mission_manager->getAll();
    $types_planques = $type_planque_manager->getAll();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KGB - Détails mission</title>
    <link rel="icon" href="./prod/assets/images/incognito.svg"/>

    <!-- Primary Meta Tags -->
    <meta name="title" content="KGB - Détails mission">
    <meta name="description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.kgb.io/">
    <meta property="og:title" content="KGB - Détails mission">
    <meta property="og:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="og:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.kgb.io/">
    <meta property="twitter:title" content="KGB - Détails mission">
    <meta property="twitter:description" content="Interface KGB. Retrouvez toutes les missions, les agents, les planques du KGB... et bien plus encore.">
    <meta property="twitter:image" content="https://ei.marketwatch.com/Multimedia/2019/06/12/Photos/ZQ/MW-HL310_trench_20190612155540_ZQ.jpg?uuid=0e3ffb42-8d4c-11e9-9b64-9c8e992d421e">


    <!-- CSS -->
    <link rel="stylesheet" href="./prod/style.css"/>
</head>

<body>
    <!-- BARRE DE NAVIGATION -->
    <nav id="home_nav" class="navbar navbar-expand-lg justify-content-between">
        <!-- LOGO -->
        <a id="home_nav_brand" class="navbar-brand" href="./home.php">KGB</a>
        <!-- ANCRES AFFICHES SEULEMENT EN DESKTOP -->
        <div id="home_nav_links" class="d-none d-md-block">
            <a href="#" class="mr-5">ADMINISTRATION</a>
            <a href="#">DECONNEXION</a>
        </div>
        <!-- BURGER AFFICHE EN TABLETTE/MOBILE -->
        <div id="burger" class="d-md-none d-block">
            <svg height="40" viewBox="0 -53 384 384" width="40" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path fill="#EDD933" d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>
        </div>
    </nav>

    <!-- MODAL MENU -->
    <div id="modal_menu">
        <div id="modal_links" class="d-none d-md-block d-flex flex-column justify-content-center">
            <div class="text-center mb-4">
                <a href="#">ADMINISTRATION</a>
            </div>
            <div class="text-center">
                <a href="#">DECONNEXION</a>
            </div>
        </div>
    </div>

    <!-- CARDS DECK -->
    <div class="container" >
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb--style">
            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
            <!-- remplacer par du php -->
            <li class="breadcrumb-item active" aria-current="page">Mission X</li>
            </ol>
        </nav>

        <div class="row">

            <div class="col-sm-12 col-md-6 order-md-2 text-right mission__date_pays">
                01/09/2016 - 10/11/2016<br/><!-- à generer en php -->
                France <!-- à generer en php -->
            </div>


            <div class="col-sm-12 col-md-6 order-md-1 mission--style">
                <h4>Mission X: <span= class="mission__text--color">X-MI6-RTY</span></h4><!-- à generer en php -->
                <h5>Nom de code: <span= class="mission__text--color">Chen</span></h5><!-- à generer en php -->
                <h5>Cible(s): <span= class="mission__text--color">csqcsq</span></h5><!-- à generer en php -->
                <h5>Type: <span= class="mission__text--color">infiltration</span></h5><!-- à generer en php -->
                <h5>Statut: <span= class="mission__text--color">en cours</span></h5><!-- à generer en php -->
            </div>
            
            <div class="col-12 order-md-3 mission--style mt-5">
                <h5>Description:</h5>
                <p><span= class="mission__text--color">Some quick example text to build on the card title and make up the bulk of the card's content.</span></p> <!-- à generer en php -->
            </div>

            <div class="col-12 order-md-4 mission--style mt-5 mb-5">
                <h5>Détails:</h5>
                <div class="mission__details--alignement mt-5">
                    <div class="mission__details__img">
                        <a href="#"><svg width="90px" height="90px "version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 25.916 25.916" style="enable-background:new 0 0 25.916 25.916;" xml:space="preserve">
                        <path style="fill:#EDD933;" d="M7.938,8.13c0.09,0.414,0.228,0.682,0.389,0.849c0.383,2.666,2.776,4.938,4.698,4.843
                        c2.445-0.12,4.178-2.755,4.567-4.843c0.161-0.166,0.316-0.521,0.409-0.938c0.104-0.479,0.216-1.201-0.072-1.583
                        c-0.017-0.02-0.127-0.121-0.146-0.138c0.275-0.992,0.879-2.762-0.625-4.353c-0.815-0.862-1.947-1.295-2.97-1.637
                        c-3.02-1.009-5.152,0.406-6.136,2.759C7.981,3.256,7.522,4.313,8.078,6.32C8.024,6.356,7.975,6.402,7.934,6.458
                        C7.645,6.839,7.833,7.651,7.938,8.13z"/>
                        <path style="fill:#EDD933;" d="M23.557,22.792c-0.084-1.835-0.188-4.743-1.791-7.122c0,0-0.457-0.623-1.541-1.037
                        c0,0-2.354-0.717-3.438-1.492l-0.495,0.339l0.055,3.218l-2.972,7.934c-0.065,0.174-0.231,0.289-0.416,0.289
                        s-0.351-0.115-0.416-0.289l-2.971-7.934c0,0,0.055-3.208,0.054-3.218c0.007,0.027-0.496-0.339-0.496-0.339
                        c-1.082,0.775-3.437,1.492-3.437,1.492c-1.084,0.414-1.541,1.037-1.541,1.037c-1.602,2.379-1.708,5.287-1.792,7.122
                        c-0.058,1.268,0.208,1.741,0.542,1.876c4.146,1.664,15.965,1.664,20.112,0C23.35,24.534,23.614,24.06,23.557,22.792z"/>
                        <path style="fill:#EDD933;" d="M13.065,14.847l-0.134,0.003c-0.432,0-0.868-0.084-1.296-0.232l1.178,1.803l-1.057,1.02
                        l1.088,6.607c0.009,0.057,0.058,0.098,0.116,0.098c0.057,0,0.106-0.041,0.116-0.098l1.088-6.607l-1.058-1.02l1.161-1.776
                        C13.888,14.756,13.487,14.83,13.065,14.847z"/>
                        </svg></a>
                        <div class="mission__details__titre mt-2">AGENT(S)</div>
                    </div>

                    <div class="mission__details__img">
                        <a href="#"><svg width="90px" height="90px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 211.621 211.621" style="enable-background:new 0 0 211.621 211.621;" xml:space="preserve">
                        <path style="fill:#EDD933;" d="M180.948,27.722C163.07,9.844,139.299-0.001,114.017,0c-4.143,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5
                        c21.276-0.001,41.279,8.284,56.324,23.329c15.046,15.045,23.331,35.049,23.33,56.326c0,4.142,3.357,7.5,7.5,7.5
                        c4.142,0,7.5-3.358,7.5-7.499C208.672,69.371,198.827,45.6,180.948,27.722z"/>
                        <path style="fill:#EDD933;" d="M150.096,94.656c0,4.142,3.358,7.5,7.501,7.499c4.142,0,7.499-3.358,7.499-7.5c-0.002-28.16-22.916-51.073-51.078-51.077
                        c-0.001,0,0,0-0.001,0c-4.142,0-7.499,3.357-7.5,7.499c0,4.142,3.357,7.5,7.499,7.501
                        C133.909,58.581,150.094,74.765,150.096,94.656z"/>
                        <path style="fill:#EDD933;" d="M133.5,132.896c-11.432-0.592-17.256,7.91-20.049,11.994c-2.339,3.419-1.463,8.086,1.956,10.425
                        c3.419,2.339,8.086,1.463,10.425-1.956c3.3-4.825,4.795-5.584,6.823-5.488c6.491,0.763,32.056,19.497,34.616,25.355
                        c0.642,1.725,0.618,3.416-0.071,5.473c-2.684,7.966-7.127,13.564-12.851,16.188c-5.438,2.493-12.105,2.267-19.276-0.651
                        c-26.777-10.914-50.171-26.145-69.531-45.271c-0.008-0.008-0.016-0.015-0.023-0.023c-19.086-19.341-34.289-42.705-45.185-69.441
                        c-2.919-7.177-3.145-13.845-0.652-19.282c2.624-5.724,8.222-10.167,16.181-12.848c2.064-0.692,3.752-0.714,5.461-0.078
                        c5.879,2.569,24.612,28.133,25.368,34.551c0.108,2.104-0.657,3.598-5.478,6.892c-3.42,2.336-4.299,7.003-1.962,10.423
                        c2.336,3.42,7.002,4.298,10.423,1.962c4.086-2.79,12.586-8.598,11.996-20.069C81.021,69.07,57.713,37.339,46.576,33.244
                        c-4.953-1.846-10.163-1.878-15.491-0.09C19.097,37.191,10.439,44.389,6.047,53.969c-4.26,9.294-4.125,20.077,0.395,31.189
                        c11.661,28.612,27.976,53.647,48.491,74.412c0.05,0.051,0.101,0.101,0.153,0.15c20.75,20.477,45.756,36.762,74.33,48.409
                        c5.722,2.327,11.357,3.492,16.746,3.492c5.074,0,9.932-1.032,14.438-3.098c9.581-4.391,16.778-13.048,20.818-25.044
                        c1.784-5.318,1.755-10.526-0.077-15.456C177.232,156.856,145.501,133.548,133.5,132.896z"/></svg></a>
                        <div class="mission__details__titre mt-2">CONTACT(S)</div>
                    </div>

                    <div class="mission__details__img">
                        <a href="#"><svg viewBox="0 0 512 512" width="90" height="90" xmlns="http://www.w3.org/2000/svg"><path fill="#EDD933" d="m498.195312 222.695312c-.011718-.011718-.023437-.023437-.035156-.035156l-208.855468-208.847656c-8.902344-8.90625-20.738282-13.8125-33.328126-13.8125-12.589843 0-24.425781 4.902344-33.332031 13.808594l-208.746093 208.742187c-.070313.070313-.140626.144531-.210938.214844-18.28125 18.386719-18.25 48.21875.089844 66.558594 8.378906 8.382812 19.445312 13.238281 31.277344 13.746093.480468.046876.964843.070313 1.453124.070313h8.324219v153.699219c0 30.414062 24.746094 55.160156 55.167969 55.160156h81.710938c8.28125 0 15-6.714844 15-15v-120.5c0-13.878906 11.289062-25.167969 25.167968-25.167969h48.195313c13.878906 0 25.167969 11.289063 25.167969 25.167969v120.5c0 8.285156 6.714843 15 15 15h81.710937c30.421875 0 55.167969-24.746094 55.167969-55.160156v-153.699219h7.71875c12.585937 0 24.421875-4.902344 33.332031-13.808594 18.359375-18.371093 18.367187-48.253906.023437-66.636719zm0 0"/></svg></a>
                        <div class="mission__details__titre mt-2">PLANQUE(S)</div>
                    </div>
                    <div class="mission__details__img">
                        <a href="#"><svg width="90" height="90" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;" xml:space="preserve">
                        <path style="fill:#EDD933;" d="M496.647,312.107l-47.061-36.8c1.459-12.844,1.459-25.812,0-38.656l47.104-36.821
                        c8.827-7.109,11.186-19.575,5.568-29.419l-48.96-84.629c-5.639-9.906-17.649-14.232-28.309-10.197l-55.467,22.315
                        c-10.423-7.562-21.588-14.045-33.323-19.349l-8.512-58.923c-1.535-11.312-11.24-19.72-22.656-19.627h-98.133
                        c-11.321-0.068-20.948,8.246-22.528,19.456l-8.533,59.093c-11.699,5.355-22.846,11.843-33.28,19.371L86.94,75.563
                        c-10.55-4.159-22.549,0.115-28.096,10.005L9.841,170.347c-5.769,9.86-3.394,22.463,5.568,29.547l47.061,36.8
                        c-1.473,12.843-1.473,25.813,0,38.656l-47.104,36.8c-8.842,7.099-11.212,19.572-5.589,29.419l48.939,84.651
                        c5.632,9.913,17.649,14.242,28.309,10.197l55.467-22.315c10.432,7.566,21.604,14.056,33.344,19.371l8.533,58.88
                        c1.502,11.282,11.147,19.694,22.528,19.648h98.133c11.342,0.091,21-8.226,22.592-19.456l8.533-59.093
                        c11.698-5.357,22.844-11.845,33.28-19.371l55.68,22.379c10.55,4.149,22.543-0.122,28.096-10.005l49.152-85.12
                        C507.866,331.505,505.447,319.139,496.647,312.107z M255.964,362.667c-58.91,0-106.667-47.756-106.667-106.667
                        s47.756-106.667,106.667-106.667s106.667,47.756,106.667,106.667C362.56,314.882,314.845,362.597,255.964,362.667z"/></svg></a>
                        <div class="mission__details__titre mt-2 ">SPECIALITÉ</div>
                    </div>
                </div>
            </div>

        </div>

        <!-- </div> -->


    <!-- FIN CARDS DECK -->
    </div>

    <!-- BOOTSTRAP -->
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- JS -->
    <script src="./prod/main.js"></script>
</body>
</html>